Want to report a bug? Please include the following information:
  * A copy of ~/Library/Preferences/com.googlecode.iterm2.plist
  * The version of iTerm2 you are running
  * OS version
  * A <a href="https://gitlab.com/gnachman/iterm2/wikis/DebugLogging">debug log</a>
  * A <a href="https://gitlab.com/gnachman/iterm2/wikis/HowToSample">sample</a> of the process, if this is a performance issue.


